# -*- encoding: utf-8 -*-
import json
from django.contrib import messages
from django.contrib.auth import logout
from django.core.files.uploadedfile import UploadedFile
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect, get_object_or_404, render
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, View, UpdateView
from core.cataloger import Cataloger, GroupInviteCatalogerStrategy, HangoutInviteCatalogerStrategy, SiteInviteCatalogerStrategy, FriendsGroupsCatalogerStrategy
from core.decorators import ajax_required
from core.facebook_integration.api import FacebookAPI, FacebookAPIException
from core.forms import EditUserForm, FriendsGroupEditForm, WallMessageEditForm, HangoutEditForm, FriendsGroupCreateForm, HangoutCreateForm
from core.invitation_sender import InvitationSender
from core.models import User, FriendsGroup, GroupInvitation, GroupPhoto, Hangout, \
    CommentOnGroupWall, CommentOnHangoutWall, ConfirmHangoutAttendance, RejectHangoutAttendance, \
    AcceptGroupInvitation, RejectGroupInvitation, InviteGroupToHangout, LeaveGroup, SiteInvitation

facebook_api = FacebookAPI()


.
.
.

class GroupWallView(GroupDetailView):
    template_name = 'group_wall.html'
    group_wall_messages_page = 'group_wall_messages_page.html'

    def get(self, request, pk):
        if request.is_ajax():
            return self.get_ajax(request, pk)
        else:
            return self.get_not_ajax(request, pk)

    def get_ajax(self, request, pk):
        self.template_name = self.group_wall_messages_page

        current_user = request.user
        group = get_object_or_404(FriendsGroup, pk=pk)

        wall_messages = group.wall.all_messages_of(current_user)
        extra_template_data = {'wall_messages': wall_messages}

        return self.render_template(request, group, extra_template_data=extra_template_data)

    def get_not_ajax(self, request, pk):
        current_user = request.user
        group = get_object_or_404(FriendsGroup, pk=pk)
        form = WallMessageEditForm()

        wall_messages = group.wall.all_messages_of(current_user)
        extra_template_data = {'form': form, 'wall_messages': wall_messages,
                               'group_wall_messages_page': self.group_wall_messages_page}

        return self.render_template(request, group, extra_template_data=extra_template_data)

    def post(self, request, pk):
        current_user = request.user
        group = get_object_or_404(FriendsGroup, pk=pk)

        form = WallMessageEditForm(request.POST)

        if group.has_member(current_user) and form.is_valid():
            message = form.cleaned_data['message']

            CommentOnGroupWall.do_with(user=current_user, group=group, message_text=message)

            return redirect('group', group.pk)

        wall_messages = group.wall.all_messages_of(current_user)
        extra_template_data = {'form': form, 'wall_messages': wall_messages,
                               'group_wall_messages_page': self.group_wall_messages_page}

        return self.render_template(request, group, extra_template_data=extra_template_data)
.
.
.