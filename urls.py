from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from core.views import *


urlpatterns = patterns('',
        url(r'^$', HomeView.as_view(), name="home"),
        url(r'^usuario/login/$', LogInView.as_view(), name="login"),
        url(r'^usuario/logout/$', LogOutView.as_view(), name="logout"),
        url(r'^usuario/login-error/$', LogInError.as_view(), name="login-error"),
        url(r'^usuario/$', login_required(ProfileView.as_view())),
        url(r'^usuario/renew-fb-session/$', login_required(RenewFacebookSession.as_view()), name="renew_fb_session"),
        .
        .
        .
        # Begin Groups
        url(r'^grupo/crear/$', login_required(GroupCreateView.as_view()), name="create_group"),
        url(r'^grupo/(?P<pk>\d+)/$', login_required(GroupWallView.as_view()), name="group"),
        url(r'^grupo/(?P<pk>\d+)/editar/$', login_required(GroupEditView.as_view()), name="edit_group"),
        url(r'^grupo/(?P<pk>\d+)/fotos/$', login_required(GroupPhotosView.as_view()),
           name="group_photos"),
        url(r'^grupo/(?P<pk>\d+)/invitar-amigos/$', login_required(GroupInviteFriendView.as_view()),
           name="group_invites"),
        .
        .
        .
        url(r'^grupo/(?P<pk>\d+)/invitar/$', login_required(GroupInviteView.as_view()), name="invite"),
        url(r'^grupo/(?P<pk>\d+)/invitados/$', login_required(GroupPossibleInvitesView.as_view()),
           name="group_possible_invites"),
        url(r'^grupo/(?P<pk>\d+)/subir-foto/$', login_required(GroupPhotoUploadView.as_view()),
           name="upload_group_photo"),
        url(r'^grupo/(?P<pk>\d+)/borrar-fotos$', login_required(GroupPhotoDeleteView.as_view()),
           name="delete_group_photo"),
        url(r'^grupo/(?P<pk>\d+)/abandonar/$', login_required(LeaveGroupView.as_view()), name="leave_group"),
        .
        .
        .
        # End Groups
        # Begin Hangouts
        url(r'^grupo/(?P<group_pk>\d+)/previa/crear/$', login_required(HangoutCreateView.as_view()),
           name="create_hangout"),
        url(r'^previa/(?P<pk>\d+)/$', login_required(HangoutWallView.as_view()),
            name="hangout"),
        url(r'^previa/(?P<pk>\d+)/editar/$', login_required(HangoutEditView.as_view()),
           name="edit_hangout"),
        url(r'^previa/(?P<pk>\d+)/invitar-grupos/$', login_required(HangoutInviteGroupView.as_view()),
           name="hangout_invites"),
        .
        .
        .
        # End Hangouts


)