from datetime import timedelta
from math import floor
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils.datetime_safe import date
from social_auth.db.django_models import UserSocialAuth
from core.facebook_integration.api import FacebookAPI, FacebookAPIException
from core.cataloger import Cataloger, GroupInviteCatalogerStrategy, SiteInviteCatalogerStrategy, FriendsGroupsCatalogerStrategy
from core.models import *
from core.models import HangoutWallCommentNotification
from core.notification_obtainer import NotificationObtainer
from core.social_pipeline import FacebookExtraDataObtainer
from core.test_utils.factories import *
from core.test_utils.utils import n_years_ago, today, random_integer, random_string, reload_object, n_days_ago
from core.user_reception import UserReception

.
.
.

class WallTest(TestCase):
    def test_user_wall_is_not_empty_when_notifications_exist(self):
        user = a_user()
        a_group_wall_comment_notification(receiver=user)

        user_wall = a_user_wall(user=user)

        self.assertTrue(len(user_wall.all_messages()) > 0)

    def test_user_wall_messages_returns_user_notifications(self):
        user = a_user()
        notification_1 = a_group_wall_comment_notification(receiver=user)
        notification_2 = a_hangout_wall_comment_notification(receiver=user)

        user_wall = a_user_wall(user=user)

        self.assertListEqual(user_wall.all_messages(), [notification_2, notification_1])

    def test_adding_message_fails_in_user_wall(self):
        user_wall = a_user_wall()

        self.assertRaises(Exception, user_wall.add_comment_from, a_user(), random_string())
        self.assertFalse(WallComment.objects.filter(wall=user_wall).exists())

    def test_group_wall_is_not_empty_when_a_message_is_added(self):
        user = a_user()
        group_wall = a_group_wall()

        group_wall.add_comment_from(sender=user, message_text=random_string())

        self.assertTrue(len(group_wall.all_messages_of(user)) == 1)

    def test_group_wall_returns_notifications_and_comments_sorted_by_date_in_descending_order(self):
        user = a_user()
        group_wall = a_group_wall()

        invitation_accepted_notification = a_group_invitation_accepted_notification_with(receiver=user,
                                                                                         group=group_wall.group)
        hangout_invitation_notification = a_hangout_invitation_notification_with(receiver=user, group=group_wall.group)

        wall_comment_1 = group_wall.add_comment_from(sender=user, message_text=random_string())
        wall_comment_2 = group_wall.add_comment_from(sender=user, message_text=random_string())

        self.assertListEqual(group_wall.all_messages_of(user),
                             [wall_comment_2, wall_comment_1, hangout_invitation_notification,
                              invitation_accepted_notification])

    def test_group_wall_does_not_returns_notifications_that_must_not_be_displayed_in_group_wall(self):
        user = a_user()
        group_wall = a_group_wall()

        notification_1 = a_group_wall_comment_notification(receiver=user, action=a_comment_on_group_wall_action_with(
            group=group_wall.group))
        notification_2 = a_hangout_wall_comment_notification(receiver=user)

        self.assertFalse(notification_1 in group_wall.all_messages_of(user))
        self.assertFalse(notification_2 in group_wall.all_messages_of(user))

    def test_can_add_messages_to_hangout_wall(self):
        user = a_user()
        hangout_wall = a_hangout_wall()

        hangout_wall.add_comment_from(sender=user, message_text=random_string())

        self.assertTrue(len(hangout_wall.all_messages_of(user)) == 1)

    def test_hangout_wall_returns_notifications_and_comments_sorted_by_date_in_descending_order(self):
        user = a_user()
        hangout_wall = a_hangout_wall()

        notification_1 = a_hangout_invitation_notification_with(receiver=user, hangout=hangout_wall.hangout)
        notification_2 = a_hangout_invitation_notification_with(receiver=user, hangout=hangout_wall.hangout)

        wall_comment_1 = hangout_wall.add_comment_from(sender=user, message_text=random_string())
        wall_comment_2 = hangout_wall.add_comment_from(sender=user, message_text=random_string())

        self.assertListEqual(hangout_wall.all_messages_of(user),
                             [wall_comment_2, wall_comment_1, notification_2, notification_1])


class CommentOnGroupWallTest(TestCase):
    def test_wall_comment_is_called_when_action_is_executed(self):
        wall = GroupWallSpy()
        user = a_user()
        group = a_group_with(wall=wall, members=[user])

        CommentOnGroupWall.do_with(user=user, group=group, message_text=random_string())

        self.assertTrue(wall.comment_was_called)

    def test_action_instance_is_created_when_action_is_executed(self):
        user = a_user()
        message_text = random_string()
        group = a_group(members=[user])

        CommentOnGroupWall.do_with(user=user, group=group, message_text=message_text)

        self.assert_a_comment_on_group_wall_action_exists_with(user=user, group=group, message_text=message_text)

    def test_no_notification_is_created_for_user_who_commented(self):
        sender = a_user()
        message_text = random_string()
        group = a_group(members=[sender])

        action = CommentOnGroupWall.do_with(user=sender, group=group, message_text=message_text)

        self.assert_a_group_wall_comment_notification_does_not_exists_with(action=action, receiver=sender)

    def tests_notifications_are_created_for_all_members_of_the_group_except_message_sender(self):
        message_text = random_string()

        sender = a_user()
        user_1 = a_user()
        user_2 = a_user()

        group = a_group(members=[sender, user_1, user_2])

        action = CommentOnGroupWall.do_with(user=sender, group=group, message_text=message_text)

        self.assert_a_group_wall_comment_notification_exists_with(action=action, receiver=user_1)
        self.assert_a_group_wall_comment_notification_exists_with(action=action, receiver=user_2)

        notification_for_user_1 = self.group_wall_comment_notification_of(action=action, receiver=user_1)
        notification_for_user_2 = self.group_wall_comment_notification_of(action=action, receiver=user_2)

        self.assertEquals(notification_for_user_1.group, group)
        self.assertEquals(notification_for_user_2.group, group)
        self.assertEquals(notification_for_user_1.wall_message.message, message_text)
        self.assertEquals(notification_for_user_2.wall_message.message, message_text)
        self.assertEquals(notification_for_user_1.generated_by, sender)
        self.assertEquals(notification_for_user_2.generated_by, sender)

    def assert_a_comment_on_group_wall_action_exists_with(self, user, group, message_text):
        self.assertTrue(
            CommentOnGroupWall.objects.filter(user=user, group=group, wall_message__message=message_text).exists())
        self.assertEquals(
            CommentOnGroupWall.objects.filter(user=user, group=group, wall_message__message=message_text).count(),
            1)

    def assert_a_group_wall_comment_notification_does_not_exists_with(self, action, receiver):
        self.assertFalse(GroupWallCommentNotification.objects.filter(action=action, receiver=receiver).exists())

    def assert_a_group_wall_comment_notification_exists_with(self, action, receiver):
        self.assertTrue(GroupWallCommentNotification.objects.filter(action=action, receiver=receiver).exists())

    def group_wall_comment_notification_of(self, action, receiver):
        return GroupWallCommentNotification.objects.get(action=action, receiver=receiver)


class CommentOnHangoutWallTest(TestCase):
    def test_wall_comment_is_called_when_action_is_executed(self):
        wall = HangoutWallSpy()
        user = a_user()
        hangout = a_hangout_with(wall=wall)

        CommentOnHangoutWall.do_with(user=user, hangout=hangout, message_text=random_string())

        self.assertTrue(wall.comment_was_called)

    def test_action_instance_is_created_when_action_is_executed(self):
        sender = a_user()
        message_text = random_string()
        hangout = a_hangout_with(groups=[a_group_with(members=[sender])])
        
        CommentOnHangoutWall.do_with(user=sender, hangout=hangout, message_text=message_text)

        self.assert_a_comment_on_hangout_wall_action_exists_with(user=sender, hangout=hangout,
                                                                 message_text=message_text)

    def test_no_notification_is_created_for_user_who_commented(self):
        sender = a_user()
        message_text = random_string()
        hangout = a_hangout_with(groups=[a_group(members=[sender])])

        action = CommentOnHangoutWall.do_with(user=sender, hangout=hangout, message_text=message_text)

        self.assert_a_hangout_wall_comment_notification_does_not_exist_with(action=action, receiver=sender)

    def tests_notifications_are_created_for_all_members_of_the_groups_in_the_hangout_except_sender(self):
        sender = a_user()
        user_1 = a_user()
        user_2 = a_user()
        message_text = random_string()
        hangout = a_hangout_with(groups=[a_group_with(members=[sender, user_1]), a_group_with(members=[user_2])])

        action = CommentOnHangoutWall.do_with(user=sender, hangout=hangout, message_text=message_text)

        self.assert_a_hangout_wall_comment_notification_exists_with(receiver=user_1, action=action)
        self.assert_a_hangout_wall_comment_notification_exists_with(receiver=user_2, action=action)

        notification_for_user_1 = self.hangout_wall_comment_notification_of(action=action, receiver=user_1)
        notification_for_user_2 = self.hangout_wall_comment_notification_of(action=action, receiver=user_2)

        self.assertEquals(notification_for_user_1.hangout, hangout)
        self.assertEquals(notification_for_user_2.hangout, hangout)
        self.assertEquals(notification_for_user_1.wall_message.message, message_text)
        self.assertEquals(notification_for_user_2.wall_message.message, message_text)
        self.assertEquals(notification_for_user_1.generated_by, sender)
        self.assertEquals(notification_for_user_2.generated_by, sender)

    def test_notifications_are_created_only_once_when_a_user_belongs_to_two_groups_of_the_hangout(self):
        user_who_comments = a_user()
        user_in_hangout = a_user()
        hangout = a_hangout_with(
            groups=[a_group_with(members=[user_who_comments, user_in_hangout]),
                    a_group_with(members=[user_in_hangout])])

        action = CommentOnHangoutWall.do_with(user=user_who_comments, hangout=hangout, message_text=random_string())

        self.assert_a_hangout_wall_comment_notification_exists_with(receiver=user_in_hangout, action=action)

        notification = self.hangout_wall_comment_notification_of(action=action, receiver=user_in_hangout)

        self.assertEquals(notification.hangout, hangout)
        self.assertEquals(notification.generated_by, user_who_comments)

    def assert_a_comment_on_hangout_wall_action_exists_with(self, user, hangout, message_text):
        self.assertTrue(CommentOnHangoutWall.objects.filter(user=user, hangout=hangout,
                                                            wall_message__message=message_text).exists())

    def assert_a_hangout_wall_comment_notification_does_not_exist_with(self, action, receiver):
        self.assertFalse(HangoutWallCommentNotification.objects.filter(action=action, receiver=receiver).exists())

    def assert_a_hangout_wall_comment_notification_exists_with(self, receiver, action):
        self.assertTrue(HangoutWallCommentNotification.objects.filter(action=action, receiver=receiver).exists())

    def hangout_wall_comment_notification_of(self, action, receiver):
        return HangoutWallCommentNotification.objects.get(action=action, receiver=receiver)

