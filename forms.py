from datetime import datetime
from datetimewidget.widgets import DateTimeWidget
from django.forms import ModelForm
from django import forms
from core.models import User, FriendsGroup, Hangout

.
.
.


class HangoutCreateForm(ModelForm):
    class Meta:
        model = Hangout
        fields = ['name', 'address', 'datetime', 'description', 'image']
        dateTimeOptions = {
            'format': 'dd/mm/yyyy HH:ii P',
            'autoclose': 'true',
            'startDate': datetime.now().strftime('%d/%m/%Y %I:%M %p'),
        }
        widgets = {
            'datetime': DateTimeWidget(options=dateTimeOptions)
        }


class HangoutEditForm(ModelForm):
    class Meta:
        model = Hangout
        fields = ['name', 'address', 'datetime', 'description']
        dateTimeOptions = {
            'format': 'dd/mm/yyyy HH:ii P',
            'autoclose': 'true',
            'startDate': datetime.now().strftime('%d/%m/%Y %I:%M %p'),
        }
        widgets = {
            'datetime': DateTimeWidget(options=dateTimeOptions)
        }