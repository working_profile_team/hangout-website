# -*- encoding: utf-8 -*-
from django.template import loader
from core.models import GroupWallCommentNotification, HangoutWallCommentNotification, GroupInvitationAcceptedNotification, HangoutAttendanceConfirmedNotification, HangoutAttendanceRejectedNotification, HangoutInvitationNotification, WallComment, UserLeftGroupNotification

from django import template

register = template.Library()


class UserWallMessageView(object):
    @classmethod
    def can_handle(cls, message):
        raise NotImplementedError

    @classmethod
    def to_handle(cls, wall_message):
        for subclass in cls.__subclasses__():
            if subclass.can_handle(wall_message):
                return subclass

        raise Exception('No view to handle %s' % wall_message)

    def template_name(self):
        raise NotImplementedError('subclass responsibility')

    def show(self, message, context):
        template = loader.get_template(self.template_name())
        context.update({'message': message})

        return template.render(context)


class GroupWallCommentNotificationView(UserWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == GroupWallCommentNotification

    def template_name(self):
        return 'messages/group_wall_comment_notification.html'


class HangoutWallCommentNotificationView(UserWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == HangoutWallCommentNotification

    def template_name(self):
        return 'messages/hangout_wall_comment_notification.html'


class GroupInvitationAcceptedNotificationUserWallView(UserWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == GroupInvitationAcceptedNotification

    def template_name(self):
        return 'messages/group_invitation_accepted_notification_user_wall.html'


class HangoutAttendanceConfirmedNotificationViewUser(UserWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == HangoutAttendanceConfirmedNotification

    def template_name(self):
        return 'messages/hangout_attendance_confirmed_notification_user_wall.html'


class HangoutAttendanceRejectedNotificationViewUser(UserWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == HangoutAttendanceRejectedNotification

    def template_name(self):
        return 'messages/hangout_attendance_rejected_notification_user_wall.html'


class UserLeftGroupNotificationUserWallView(UserWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == UserLeftGroupNotification

    def template_name(self):
        return 'messages/user_left_group_notification.html'


@register.simple_tag(takes_context=True)
def show_user_wall_message(context, wall_message):
    view_class_for_message = UserWallMessageView.to_handle(wall_message)

    view_for_message = view_class_for_message()
    rendered_view = view_for_message.show(wall_message, context)

    mark_as_read(wall_message)

    return rendered_view


class GroupWallMessageView(object):
    @classmethod
    def can_handle(cls, message):
        raise NotImplementedError

    @classmethod
    def to_handle(cls, wall_message):
        for subclass in cls.__subclasses__():
            if subclass.can_handle(wall_message):
                return subclass

        raise Exception('No view to handle %s' % wall_message)

    def template_name(self):
        raise NotImplementedError('subclass responsibility')

    def show(self, message, context):
        template = loader.get_template(self.template_name())
        context.update({'message': message})

        return template.render(context)


class WallCommentGroupWallView(GroupWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == WallComment

    def template_name(self):
        return 'messages/group_wall_comment.html'


class GroupInvitationAcceptedNotificationGroupWallView(GroupWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == GroupInvitationAcceptedNotification

    def template_name(self):
        return 'messages/group_invitation_accepted_notification_group_wall.html'


class HangoutInvitationNotificationGroupWallView(GroupWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == HangoutInvitationNotification

    def template_name(self):
        return 'messages/hangout_invitation_notification_group_wall.html'


class UserLeftGroupNotificationGroupWallView(GroupWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == UserLeftGroupNotification

    def template_name(self):
        return 'messages/user_left_group_notification_group_wall.html'


@register.simple_tag(takes_context=True)
def show_group_wall_message(context, wall_message):
    view_class_for_message = GroupWallMessageView.to_handle(wall_message)

    view_for_message = view_class_for_message()
    rendered_view = view_for_message.show(wall_message, context)

    mark_as_read(wall_message)

    return rendered_view


class HangoutWallMessageView(object):
    @classmethod
    def can_handle(cls, message):
        raise NotImplementedError

    @classmethod
    def to_handle(cls, wall_message):
        for subclass in cls.__subclasses__():
            if subclass.can_handle(wall_message):
                return subclass

        raise Exception('No view to handle %s' % wall_message)

    def template_name(self):
        raise NotImplementedError('subclass responsibility')

    def show(self, message, context):
        template = loader.get_template(self.template_name())
        context.update({'message': message})

        return template.render(context)


class WallCommentHangoutWallView(HangoutWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == WallComment

    def template_name(self):
        return 'messages/hangout_wall_comment.html'


class HangoutAttendanceConfirmedNotificationHangoutWallView(HangoutWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == HangoutAttendanceConfirmedNotification

    def template_name(self):
        return 'messages/hangout_attendance_confirmed_notification_hangout_wall.html'


class HangoutAttendanceRejectedNotificationHangoutWallView(HangoutWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == HangoutAttendanceRejectedNotification

    def template_name(self):
        return 'messages/hangout_attendance_rejected_notification_hangout_wall.html'


class HangoutInvitationNotificationHangoutWallView(HangoutWallMessageView):
    @classmethod
    def can_handle(cls, message):
        return type(message) == HangoutInvitationNotification

    def template_name(self):
        return 'messages/hangout_invitation_notification_hangout_wall.html'


def mark_as_read(wall_message):
    if hasattr(wall_message, 'read') and not wall_message.read:
        wall_message.mark_as_read()


@register.simple_tag(takes_context=True)
def show_hangout_wall_message(context, wall_message):
    view_class_for_message = HangoutWallMessageView.to_handle(wall_message)

    view_for_message = view_class_for_message()
    rendered_view = view_for_message.show(wall_message, context)
    
    mark_as_read(wall_message)

    return rendered_view