from django import template

register = template.Library()


@register.simple_tag(takes_context=True)
def group_image_with_default(context, image):
    return image_or_static_default(image, 'group_default.jpg', context)


@register.simple_tag(takes_context=True)
def default_profile_picture(context):
    return context['STATIC_URL'] + 'img/defecto-usuario.jpg'


@register.simple_tag(takes_context=True)
def default_group_picture(context):
    return context['STATIC_URL'] + 'img/group_default.jpg'


# Begin Hangout Tags
@register.simple_tag(takes_context=True)
def default_hangout_picture(context):
    return context['STATIC_URL'] + 'img/hangout_default.jpg'


@register.simple_tag(takes_context=True)
def hangout_image_with_default(context, image):
    return image_or_static_default(image, 'hangout_default.jpg', context)
# End Hangout Tags


def image_or_static_default(image, default, context):
    if not image:
        return context['STATIC_URL'] + 'img/' + default

    return context['MEDIA_URL'] + image.name
