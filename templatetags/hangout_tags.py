from django import template

register = template.Library()

@register.simple_tag(takes_context=False)
def hangout_confirmed_proportion(confirmed_count, total_count):
    return "{0:.0f}%".format(float(confirmed_count)/total_count * 100)


# def confirmed_attendances(hangout):
#     HangoutConfirmationAttendance.objects.confirmed_attendances().filter(hangout=hangout)
#
#
# def rejected_attendances(hangout):
#     HangoutConfirmationAttendance.objects.rejected_attendances().filter(hangout=hangout)
#
#
# def pending_attendances(hangout):
#     HangoutConfirmationAttendance.objects.pending_attendances().filter(hangout=hangout)

@register.assignment_tag(takes_context=False)
def group_has_user(group, user):
    return group.has_member(user)

@register.assignment_tag(takes_context=False)
def dict_get(dict, key):
    return dict.get(key)