# -*- encoding: utf-8 -*-
from datetime import timedelta
from math import floor
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.utils.datetime_safe import date, datetime
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db import models
from sorl.thumbnail import get_thumbnail
from core.managers import GroupInvitationManager, UserManager, PendingActionManager, HangoutInvitationManager, \
    FriendsGroupManager, HangoutConfirmationAttendanceManager, HangoutManager
from core.notification_obtainer import NotificationObtainer

GENDER_CHOICES = (
    ('M', 'Hombre'),
    ('F', 'Mujer'),
)

INVITATION_STATUS_CHOICES = (
    ('P', 'Pendiente'),
    ('A', 'Aceptada'),
    ('R', 'Rechazada'),
)

RESPONSE_INVITATION_STATUS_CHOICES = (
    ('P', 'Pendiente'),
    ('C', 'Confirmado'),
    ('R', 'Rechazado'),
)



class FriendsGroup(models.Model):
    name = models.CharField(_(u'nombre'), max_length=200)
    area = models.CharField(_(u'zona'), max_length=200, blank=True)
    description = models.TextField(_(u'descripción'), max_length=1000, blank=True)
    members = models.ManyToManyField(User, related_name='friends_groups')
    image = models.ImageField(_(u'imagen'), upload_to='group_images', null=True, blank=True)
    wall = models.OneToOneField('core.GroupWall', related_name='group', null=True, blank=True)
    deleted = models.BooleanField(_(u'eliminado'), default=False)

    objects = FriendsGroupManager()

    def __unicode__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.wall:
            self.wall = GroupWall.new()

        return super(FriendsGroup, self).save(force_insert, force_update, using, update_fields)

    def remove(self):
        self.pending_invitations.all().delete()
        self.invitations.pending_invitations().delete()

        self.deleted = True
        self.save()

    def get_members(self):
        return self.members.all()

    def has_member(self, user):
        return self.members.filter(pk=user.pk).exists()

    def member_count(self):
        return self.members.count()

    def add_member(self, user):
        if self.has_member(user):
            raise Exception('User ' + str(user) + ' is already in group')

        self.members.add(user)

    def remove_member(self, user):
        if not self.has_member(user):
            raise Exception('User is not in group')

        if self.member_count() == 1 and self.has_created_a_hangout():
            raise Exception('Last member cannot leave when group created a hangout')

        self.members.remove(user)

        if self.members.count() == 0:
            self.remove()

    def age_average(self):
        all_members = self.get_members()
        member_count = 0
        age_sum = 0
        age_average = 0

        for member in all_members:
            if member.age:
                age_sum += member.age
                member_count += 1

        if member_count > 0:
            age_average = int(floor(age_sum/member_count))

        return age_average

    def all_pending_invitations(self):
        return self.invitations.filter(status='P')

    def all_pending_invitations_to_send(self):
        return self.pending_invitations.not_completed()

    def images(self):
        return self.image_library.all()

    def has_created_a_hangout(self):
        return self.created_hangouts.active().exists()

    def get_hangouts(self):
        return self.hangouts.active().all()

    def change_image(self, image_file):
        self.image.save(str(image_file), image_file)

    def get_image_thumbnail(self):
        im = get_thumbnail(self.image, "60x60", quality=90)
        return im.url


class GroupPhoto(models.Model):
    group = models.ForeignKey(FriendsGroup, related_name='image_library')
    image = models.ImageField(_(u'imagen'), upload_to='group_images')
    upload_date = models.DateTimeField(_(u'fecha de carga'), auto_now_add=True)

    def __unicode__(self):
        return u'Image: %s for: %s' % (self.image, self.group)

    @property
    def filename(self):
        return self.image.name

    @property
    def size(self):
        return self.image.size

    @property
    def url(self):
        return self.image.url

    def get_thumbnail(self):
        im = get_thumbnail(self.image, "80x80", quality=90)
        return im.url

    @classmethod
    def new_from_file(cls, file, group):
        photo = cls(group=group)
        photo.image.save(str(file), file)

        photo.save()

        return photo

.
.
.

class Wall(models.Model):
    def add_comment_from(self, sender, message_text):
        return WallComment.new_with(wall=self, sender=sender, message=message_text)

    @classmethod
    def new(cls):
        wall = cls()

        wall.full_clean()
        wall.save()

        return wall


class UserWall(Wall):
    class Meta:
        proxy = True

    def __init__(self, *args, **kwargs):
        self.notification_manager = NotificationObtainer(
            notification_types=[GroupWallCommentNotification, HangoutWallCommentNotification,
                                GroupInvitationAcceptedNotification, HangoutAttendanceConfirmedNotification,
                                HangoutAttendanceRejectedNotification, UserLeftGroupNotification])

        super(UserWall, self).__init__(*args, **kwargs)

    def add_comment_from(self, sender, message_text):
        raise Exception('Cannot comment on user wall')

    def all_messages(self):
        return self.notification_manager.get_all_notifications_of(self.user)

    def get_n_messages(self, n):
        return self.notification_manager.get_n_notifications_of(self.user, n=n)


class GroupWall(Wall):
    class Meta:
        proxy = True

    def __init__(self, *args, **kwargs):
        self.notification_manager = NotificationObtainer(
            notification_types=[GroupInvitationAcceptedNotification, HangoutInvitationNotification,
                                UserLeftGroupNotification])

        super(GroupWall, self).__init__(*args, **kwargs)

    def notifications_of(self, user):
        return self.notification_manager.get_all_notifications_of(user, action__group=self.group)

    def all_comments(self):
        return list(self.comments.all())

    def _sort_messages(self, messages):
        messages.sort(key=lambda message: message.date, reverse=True)

    def all_messages_of(self, user):
        notifications = self.notifications_of(user)
        comments = self.all_comments()

        messages = notifications + comments

        self._sort_messages(messages)

        return messages


class HangoutWall(Wall):
    class Meta:
        proxy = True

    def __init__(self, *args, **kwargs):
        self.notification_manager = NotificationObtainer(
            notification_types=[HangoutAttendanceConfirmedNotification, HangoutAttendanceRejectedNotification,
                                HangoutInvitationNotification])

        super(HangoutWall, self).__init__(*args, **kwargs)

    def notifications_of(self, user):
        return self.notification_manager.get_all_notifications_of(user, action__hangout=self.hangout)

    def all_comments(self):
        return list(self.comments.all())

    def _sort_messages(self, messages):
        messages.sort(key=lambda message: message.date, reverse=True)

    def all_messages_of(self, user):
        notifications = self.notifications_of(user)
        comments = self.all_comments()

        messages = notifications + comments

        self._sort_messages(messages)

        return messages


class WallComment(models.Model):
    wall = models.ForeignKey(Wall, related_name='comments')
    sender = models.ForeignKey(User, related_name='wall_comments')
    message = models.TextField(_(u'texto'), max_length=2000, blank=False)
    date = models.DateTimeField(_(u'fecha'), auto_now=True)

    def __unicode__(self):
        return u'Message of %s in %s ' % (self.sender, self.wall)

    @classmethod
    def new_with(cls, wall, sender, message):
        message = cls(wall=wall, sender=sender, message=message)

        message.full_clean()
        message.save()

        return message
